import { GraphQLServer } from 'graphql-yoga'
import { Prisma } from 'prisma-binding'
import path from 'path'
import {
  fileLoader,
  mergeResolvers,
} from 'merge-graphql-schemas'

import getUser from './utils/authentication'

require('dotenv').config()

const resolvers = mergeResolvers(fileLoader(path.join(__dirname, './resolvers')))

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  config: './graphqlconfig.yml',
  resolvers,
  context: req => ({
    ...req,
    user: getUser(req),
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql', // the auto-generated GraphQL schema of the Prisma API
      endpoint: process.env.PRISMA_ENDPOINT, // the endpoint of the Prisma API
      debug: false, // log all GraphQL queries & mutations sent to the Prisma API
      secret: process.env.PRISMA_MANAGEMENT_API_SECRET, // only needed if specified in `database/prisma.yml`
    }),
  }),
})

const options = {
  port: process.env.SERVER_PORT,
  cors: {
    origin: process.env.APP_URL,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
  },
}

console.log(process.env.APP_URL)

server.start(options, () => console.log(`Server is running on ${process.env.PRISMA_ENDPOINT}`))
