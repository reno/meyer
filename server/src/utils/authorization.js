
const createResolver = (resolver) => {
  const baseResolver = resolver
  baseResolver.createResolver = (childResolver) => {
    const newResolver = async (parent, args, context, info) => {
      await resolver(parent, args, context, info)
      return childResolver(parent, args, context, info)
    }
    return createResolver(newResolver)
  }
  return baseResolver
}

export const requiresAuth = createResolver(async (parent, args, { db, user }, info) => {
  if (!user || !user.id) {
    throw new Error('Not authenticated')
  }

  if (!user.admin) {
    const employer = await db.query.employer({ where: { id: user.id } }, info)

    if (!employer) {
      throw new Error('Not authenticated')
    }
  }
})

export const requiresAdmin = createResolver((parent, args, { user }) => {
  if (!user || !user.id) {
    throw new Error('Not authenticated')
  }

  if (!user.admin) {
    throw new Error('Not authorized')
  }
})
