import jwt from 'jsonwebtoken'

export default (req) => {
  const token = req.request.get('x-token')

  if (token) {
    try {
      const { user } = jwt.verify(token, process.env.APP_SECRET)
      return user
    } catch (err) {
      return false
    }
  }
  return false
}
