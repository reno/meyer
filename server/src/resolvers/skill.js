import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    skills: requiresAuth.createResolver((parent, args, ctx) =>
      ctx.db.query.skills({ orderBy: 'order_ASC' })),
  },

  Mutation: {
    createSkill: requiresAdmin.createResolver(async (parent, args, ctx) => {
      const count = await ctx.db.query.skills('id')
      return ctx.db.mutation.createSkill({
        data: {
          ...args,
          order: Math.max((count.length + 1), 1),
        },
      })
    }),

    updateSkill: requiresAdmin.createResolver((parent, { id, ...data }, ctx) =>
      ctx.db.mutation.updateSkill({
        where: { id },
        data,
      })),

    deleteSkill: requiresAdmin.createResolver((parent, { id }, ctx) =>
      ctx.db.mutation.deleteSkill({ where: { id } })),

    updateSkillOrder: requiresAdmin.createResolver((parent, { elements }, ctx) => {
      elements.forEach((element, index) => (
        ctx.db.mutation.updateSkill({
          where: { id: element },
          data: { order: index },
        })
      ))
      return true
    }),
  },
}
