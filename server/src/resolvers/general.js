import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    generals: requiresAuth.createResolver((parent, args, ctx) => (
      ctx.db.query.generals({ orderBy: 'title_ASC' })
    )),
  },

  Mutation: {
    createGeneral: requiresAdmin.createResolver((parent, args, ctx) =>
      ctx.db.mutation.createGeneral({ data: args })),

    updateGeneral: requiresAdmin.createResolver((parent, { id, ...data }, ctx) =>
      ctx.db.mutation.updateGeneral({
        where: { id },
        data,
      })),

    deleteGeneral: requiresAdmin.createResolver((parent, { id }, ctx) =>
      ctx.db.mutation.deleteGeneral({ where: { id } })),
  },
}
