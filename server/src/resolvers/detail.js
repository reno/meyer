import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    details: requiresAuth.createResolver((parent, args, ctx, info) =>
      ctx.db.query.details({ orderBy: 'order_ASC' }, info)),
  },

  Mutation: {
    createDetail: requiresAdmin.createResolver(async (parent, args, ctx) => {
      const count = await ctx.db.query.details('id')

      return ctx.db.mutation.createDetail({
        data: {
          ...args,
          order: Math.max((count.length + 1), 1),
        },
      })
    }),

    updateDetail: requiresAdmin.createResolver((parent, { id, ...data }, ctx) =>
      ctx.db.mutation.updateDetail({
        where: { id },
        data,
      })),

    updateDetailOrder: requiresAdmin.createResolver((parent, { elements }, ctx) => {
      elements.forEach((element, index) => (
        ctx.db.mutation.updateDetail({
          where: { id: element },
          data: { order: index },
        })
      ))
      return true
    }),

    deleteDetail: requiresAdmin.createResolver((parent, { id }, ctx) =>
      ctx.db.mutation.deleteDetail({ where: { id } })),
  },
}
