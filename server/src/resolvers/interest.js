import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    interests: requiresAuth.createResolver((parent, args, ctx) =>
      ctx.db.query.interests({ orderBy: 'order_ASC' })),
  },

  Mutation: {
    createInterest: requiresAdmin.createResolver(async (parent, args, ctx) => {
      const count = await ctx.db.query.interests('id')
      return ctx.db.mutation.createInterest({
        data: {
          ...args,
          order: Math.max((count.length + 1), 1),
        },
      })
    }),

    updateInterest: requiresAdmin.createResolver((parent, { id, ...fields }, ctx) => (
      ctx.db.mutation.updateInterest({
        where: { id },
        data: { ...fields },
      })
    )),

    deleteInterest: requiresAdmin.createResolver((parent, { id }, ctx) => ctx.db.mutation.deleteInterest({
      where: { id },
    })),

    updateInterestOrder: requiresAdmin.createResolver((parent, { elements }, ctx) => {
      elements.map((element, index) => (
        ctx.db.mutation.updateInterest({
          where: { id: element },
          data: { order: index },
        })
      ))
      return true
    }),
  },
}
