import jwt from 'jsonwebtoken'
import moment from 'moment'

import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    employer: requiresAuth.createResolver((parent, args, { user: { id }, db }, info) =>
      db.query.employer({ where: { id } }, info)),

    employers: requiresAdmin.createResolver((parent, args, ctx, info) =>
      ctx.db.query.employers({ orderBy: 'updatedAt_DESC' }, info)),
  },

  Mutation: {
    createEmployer: requiresAdmin.createResolver((parent, data, ctx) =>
      ctx.db.mutation.createEmployer({ data })),

    deleteEmployer: requiresAdmin.createResolver((parent, { id }, ctx) =>
      ctx.db.mutation.deleteEmployer({ where: { id } })),

    getEmployerToken: requiresAdmin.createResolver((parent, { id, expiration }) =>
      jwt.sign({
        user: { id },
        exp: (moment(expiration) / 1000),
      }, process.env.APP_SECRET)),

    updateEmployer: requiresAdmin.createResolver((parent, { id, ...data }, ctx) =>
      ctx.db.mutation.updateEmployer({
        where: { id },
        data,
      })),

    updateEmployerLikes: requiresAuth.createResolver(async (parent, { id, fieldName, likeId }, ctx) => {
      const likes = await ctx.db.query.employer({
        where: { id },
      }, `{${fieldName}}`)

      const likeIndex = likes[fieldName].indexOf(likeId)

      if (likeIndex >= 0) {
        likes[fieldName].splice(likeIndex, 1)
      } else {
        likes[fieldName].push(likeId)
      }

      return ctx.db.mutation.updateEmployer({
        where: { id },
        data: { [fieldName]: { set: likes[fieldName] } },
      })
    }),
  },
}
