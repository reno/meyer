import {
  requiresAdmin,
  requiresAuth,
} from '../utils/authorization'

export default {
  Query: {
    experiences: requiresAuth.createResolver((parent, args, ctx) =>
      ctx.db.query.experiences({ orderBy: 'order_ASC' })),
  },

  Mutation: {
    createExperience: requiresAdmin.createResolver(async (parent, args, ctx) => {
      const count = await ctx.db.query.experiences('id')
      return ctx.db.mutation.createExperience({
        data: {
          ...args,
          order: Math.max((count.length + 1), 1),
        },
      })
    }),

    updateExperience: requiresAdmin.createResolver((parent, { id, ...data }, ctx) =>
      ctx.db.mutation.updateExperience({
        where: { id },
        data,
      })),

    updateExperienceOrder: requiresAdmin.createResolver((parent, { elements }, ctx) => {
      elements.forEach((element, index) => (
        ctx.db.mutation.updateExperience({
          where: { id: element },
          data: { order: index },
        })
      ))
      return true
    }),

    deleteExperience: requiresAdmin.createResolver((parent, { id }, ctx) =>
      ctx.db.mutation.deleteExperience({ where: { id } })),
  },
}
