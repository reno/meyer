import { createMuiTheme } from '@material-ui/core/styles'
import blueGrey from '@material-ui/core/colors/blueGrey'

export default createMuiTheme({
  palette: {
    primary: blueGrey,
  },
  typography: {
    fontFamily: [
      'Barlow Semi Condensed',
      'Roboto',
      'Helvetica',
      'Arial',
      'sans-serif',
    ].join(', '),
    display4: {
      fontWeight: 100,
    },
  },
})
