import { combineReducers } from 'redux'
import core from '../modules/core/reducers'
import experience from '../modules/experience/reducers'

export default combineReducers({
  core,
  experience,
})
