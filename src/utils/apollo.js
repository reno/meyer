import { createHttpLink } from 'apollo-link-http'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link'

const httpLink = createHttpLink({ uri: process.env.REACT_APP_SERVER })

const tokenLink = new ApolloLink((operation, forward) => {
  const context = operation.getContext()
  operation.setContext({
    headers: {
      ...context.headers,
      'x-token': localStorage.getItem('token'),
    },
  })

  return forward(operation)
})

const link = tokenLink.concat(httpLink)

export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
})
