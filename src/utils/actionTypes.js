// Core
export const SET_NAV_INDEX = 'SET_NAV_INDEX'

export const OPEN_SNACKBAR = 'OPEN_SNACKBAR'
export const CLOSE_SNACKBAR = 'CLOSE_SNACKBAR'

export const SET_ERROR = 'SET_ERROR'

export const TOGGLE_COLLAPSE = 'TOGGLE_COLLAPSE'
export const SET_FILTER = 'SET_FILTER'
