import gql from 'graphql-tag'

// eslint-disable-next-line
export const getExperiencesQuery = gql`
  query {
    experiences {
      id
      type
      period
      title
      institution
      link
      description
    }
    employer {
      id
      experienceLikes
    }
  }
`
