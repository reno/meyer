import { combineReducers } from 'redux'
import * as types from '../../utils/actionTypes'
import { createReducer } from '../../utils/helpers'

const initialState = {
  collapse: false,
  filter: '',
}

// Collapse
const toggleCollapse = state => !state

export const collapse = createReducer(initialState.collapse, {
  [types.TOGGLE_COLLAPSE]: toggleCollapse,
})

// Filter
const setFilter = (state, payload) => (
  (state === payload) ? '' : payload
)

export const filter = createReducer(initialState.filter, {
  [types.SET_FILTER]: setFilter,
})

// Default Export
export default combineReducers({
  collapse,
  filter,
})
