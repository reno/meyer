import { createSelector } from 'reselect'

export const getCollapse = createSelector(
  [
    state => state.experience.collapse,
  ],
  collapse => collapse,
)
export const getFilter = createSelector(
  [
    state => state.experience.filter,
  ],
  filter => filter,
)
