import gql from 'graphql-tag'

// eslint-disable-next-line
export const updateEmployerLikesMutation = gql`
  mutation updateEmployerLikesMutation($id: ID!, $fieldName: String! $likeId: ID!){
    updateEmployerLikes(id: $id, fieldName: $fieldName, likeId: $likeId) {
      id
    }
  }
`
