import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Icon from '@material-ui/core/Icon'

const ExperienceFilter = (props) => {
  const {
    classes,
    collapse,
    filter,
    filters,
    setFilter,
    toggleCollapse,
  } = props

  return (
    <Grid container justify="center">
      <Grid item>
        {filters && filters.map(fil => (
          <Button
            className={classes.button}
            color={(filter === fil.name) ? 'secondary' : 'primary'}
            key={fil.name}
            onClick={() => setFilter(fil.name)}
            variant="outlined"
          >
            <Icon className={classes.root}>{fil.icon}</Icon>
            {fil.name}
          </Button>
        ))}
        <Button
          className={classes.button}
          color={(collapse) ? 'secondary' : 'primary'}
          key="collapse"
          onClick={toggleCollapse}
          variant="outlined"
        >
          <Icon>vertical_align_center</Icon>
        </Button>
      </Grid>
    </Grid>
  )
}

const styles = () => ({
  button: {
    margin: 20,
  },
  root: {
    marginRight: 15,
  },
})

export default withStyles(styles)(ExperienceFilter)
