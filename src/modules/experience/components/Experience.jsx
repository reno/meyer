import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Collapse from '@material-ui/core/Collapse'

import {
  AuthorizationWrapper,
  CenterWrapper,
  GetInTouch,
} from '../../core/'

import {
  ExperienceFilter,
  ExperienceItem,
} from '../'

const Experience = (props) => {
  const {
    data: {
      loading,
      experiences,
      employer,
      error,
    },
    filter,
    inPlace,
  } = props

  return (
    <AuthorizationWrapper error={error}>
      <CenterWrapper>
        <Grid item xs={12}>
          <ExperienceFilter />
        </Grid>
        <Grid item xs={12} sm={10} md={8}>
          {!loading && experiences.map((experience, index) => (
            <Collapse
              key={experience.id}
              in={!filter || (filter === experience.type)}
            >
              <div>
                <ExperienceItem
                  employer={employer}
                  experience={experience}
                  index={index}
                  inPlace={inPlace}
                />
              </div>
            </Collapse>
          ))}
        </Grid>
      </CenterWrapper>
      <CenterWrapper>
        <GetInTouch
          inPlace={inPlace}
          text="Let's talk"
        />
      </CenterWrapper>
    </AuthorizationWrapper>
  )
}

const styles = () => ({
  title: {
    flex: 1,
  },
})

export default withStyles(styles)(Experience)
