import React from 'react'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Chip from '@material-ui/core/Chip'
import Collapse from '@material-ui/core/Collapse'
import Grid from '@material-ui/core/Grid'
import Grow from '@material-ui/core/Grow'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'

import { HtmlParser } from '../../core'

const ExperienceItem = (props) => {
  const {
    classes,
    collapse,
    employer,
    experience,
    index,
    inPlace,
    openSnackBar,
    theme: { transitions: { duration: { shortest } } },
    updateEmployerLikesMutation,
  } = props

  const liked = employer && employer.experienceLikes && employer.experienceLikes.includes(experience.id)

  const updateEmployerLikes = () => {
    openSnackBar(`${liked ? "Ok, you don't like my" : 'Cool, so you like my'} ${experience.title} experience`)
    updateEmployerLikesMutation({
      variables: {
        id: employer.id,
        fieldName: 'experienceLikes',
        likeId: experience.id,
      },
    })
  }

  return (
    <React.Fragment>
      <Grid
        container
        className={classes.experience}
        alignItems="center"
      >
        <Grid
          className={classes.period}
          item
          xs={12}
          sm={4}
          md={3}
          lg={2}
        >
          <Chip label={experience.period} />
        </Grid>
        <Grid
          className={classes.title}
          item
          xs={12}
          sm={8}
          md={9}
          lg={10}
        >
          <Typography
            className={classes.root}
            variant="title"
          >
            <span>{experience.title} @ </span>
            {experience.link ? (
              <a href={experience.link} target="_blank">{experience.institution}</a>
            ) : (
              <span>{experience.institution}</span>
            )}
          </Typography>
          <div className={classes.button}>
            <IconButton
              onClick={updateEmployerLikes}
            >
              <Icon color={liked ? 'secondary' : 'primary'}>thumb_up</Icon>
            </IconButton>
          </div>
        </Grid>
      </Grid>
      <Grid container className={classes.experience}>
        <Grid
          className={classes.period}
          item
          xs={12}
          sm={4}
          md={3}
          lg={2}
        />
        <Grid
          className={classes.item}
          item
          xs={12}
          sm={8}
          md={9}
          lg={10}
        >
          <Grow
            in={inPlace}
            {...(inPlace && { timeout: (shortest * (index + 1)) })}
          >
            <div>
              <Collapse
                in={!collapse}
              >
                <div>
                  <HtmlParser
                    content={experience.description}
                  />
                </div>
              </Collapse>
            </div>
          </Grow>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

const styles = theme => ({
  experience: {},
  period: {
    textAlign: 'center',
    background: `linear-gradient(to right, 
      transparent 0%, 
      transparent calc(50% - 0.8px), 
      ${theme.palette.primary.dark} calc(50% - 0.8px), 
      ${theme.palette.primary.dark} calc(50% + 0.8px), 
      transparent calc(50% + 0.81px), 
      transparent 100%)`,
  },
  title: {
    display: 'flex',
    alignItems: 'baseline',
  },
  button: {
    marginLeft: 20,
  },
  item: {
    marginBottom: 30,
  },
  root: {
    display: 'inline-block',
    color: theme.palette.primary.dark,
    '& a': {
      color: theme.palette.secondary.dark,
      textDecoration: 'none',
    },
  },
  label: {},
})

export default withStyles(styles)(withTheme()(ExperienceItem))
