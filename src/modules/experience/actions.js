import * as types from '../../utils/actionTypes'


// Collapse
const toggleCollapseAction = () => ({
  type: types.TOGGLE_COLLAPSE,
})

export const toggleCollapse = () =>
  dispatch => dispatch(toggleCollapseAction())


// Filter
const setFilterAction = filter => ({
  type: types.SET_FILTER,
  payload: filter,
})

export const setFilter = filter =>
  dispatch => dispatch(setFilterAction(filter))
