import { connect } from 'react-redux'
import {
  compose,
  graphql,
} from 'react-apollo'
import { getExperiencesQuery } from '../queries'
import { updateEmployerLikesMutation } from '../mutations'
import { openSnackBar } from '../../core/actions'
import { getCollapse } from '../selectors'
import ExperienceItem from '../components/ExperienceItem'

const mapStateToProps = state => ({
  collapse: getCollapse(state),
})

const mapDispatchToProps = dispatch => ({
  openSnackBar: message => dispatch(openSnackBar(message)),
})

export default compose(
  graphql(updateEmployerLikesMutation, {
    name: 'updateEmployerLikesMutation',
    options: {
      refetchQueries: [{
        query: getExperiencesQuery,
      }],
    },
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ExperienceItem)

