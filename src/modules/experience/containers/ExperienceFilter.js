import { connect } from 'react-redux'
import {
  setFilter,
  toggleCollapse,
} from '../actions'
import {
  getCollapse,
  getFilter,
} from '../selectors'
import ExperienceFilter from '../components/ExperienceFilter'

const mapStateToProps = state => ({
  collapse: getCollapse(state),
  filter: getFilter(state),
  filters: [
    {
      name: 'PROFESSIONAL',
      icon: 'business_center',
    },
    {
      name: 'EDUCATIONAL',
      icon: 'school',
    },
  ],
})

const mapDispatchToProps = dispatch => ({
  setFilter: message => dispatch(setFilter(message)),
  toggleCollapse: collapse => dispatch(toggleCollapse(collapse)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExperienceFilter)
