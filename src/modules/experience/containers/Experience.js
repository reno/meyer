import { connect } from 'react-redux'
import {
  compose,
  graphql,
} from 'react-apollo'
import { getExperiencesQuery } from '../queries'
import { setFilter } from '../actions'
import { getFilter } from '../selectors'
import Experience from '../components/Experience'

const mapStateToProps = state => ({
  filter: getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: message => dispatch(setFilter(message)),
})

export default compose(
  graphql(getExperiencesQuery),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Experience)
