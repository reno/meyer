import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import blueGrey from '@material-ui/core/colors/blueGrey'
import Chip from '@material-ui/core/Chip'
import Icon from '@material-ui/core/Icon'
import Tooltip from '@material-ui/core/Tooltip'

const InterestItem = (props) => {
  const {
    classes,
    interest,
    employer,
    openSnackBar,
    updateEmployerLikesMutation,
  } = props


  const liked = employer && employer.interestLikes && employer.interestLikes.includes(interest.id)

  const updateEmployerLikes = () => {
    openSnackBar(`${liked ? "Ok, you don't like" : 'Cool, you also like'} ${interest.title}`)
    updateEmployerLikesMutation({
      variables: {
        id: employer.id,
        fieldName: 'interestLikes',
        likeId: interest.id,
      },
    })
  }


  return (
    <Tooltip title={`Do you also like ${interest.title}?`}>
      <Chip
        label={interest.title}
        className={`${classes.root} ${liked && 'liked'}`}
        {...(employer && { onClick: updateEmployerLikes })}
        {...(employer && { onDelete: updateEmployerLikes })}
        {...(employer && { deleteIcon: (<Icon className={classes.deleteIcon} color={liked ? 'secondary' : 'primary'}>thumb_up</Icon>) })}
      />
    </Tooltip>
  )
}

const styles = theme => ({
  root: {
    margin: 10,
    backgroundColor: blueGrey[50],
    '&.liked': {
      '& $deleteIcon': {
        color: theme.palette.secondary.main,
      },
    },
  },
  deleteIcon: {},
})

export default withStyles(styles)(InterestItem)
