import React from 'react'
import copy from 'copy-to-clipboard'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grow from '@material-ui/core/Grow'
import Icon from '@material-ui/core/Icon'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'

const Detail = (props) => {
  const {
    classes,
    detail,
    index,
    inPlace,
    openSnackBar,
    theme: { transitions: { duration: { shortest } } },
  } = props

  return (
    <Grow
      in={inPlace}
      {...(inPlace && { timeout: (shortest * (index + 1)) })}
    >
      <ListItem
        className={classes.item}
        {...(!!detail.link && {
          button: true,
          component: 'a',
          href: detail.link,
        })}
      >
        <ListItemIcon>
          <Icon className={classes.icon}>
            {detail.icon}
          </Icon>
        </ListItemIcon>
        <ListItemText primary={detail.title} />
        {!!detail.link && (
          <Tooltip
            id={detail.icon}
            title={`copy "${detail.title}" to clipboard`}
          >
            <Button
              className={classes.copy}
              onClick={(event) => {
                event.preventDefault()
                copy(detail.title)
                openSnackBar(`${detail.title} has been copied to the clipboard!`)
              }}
            >
              <Icon>share</Icon>
            </Button>
          </Tooltip>
        )}
      </ListItem>
    </Grow>
  )
}

const styles = theme => ({
  copy: {
    opacity: 0.2,
    transition: `opacity ${theme.transitions.duration.short}ms ${theme.transitions.easing.easeIn}`,
    '&:hover': {
      opacity: 1,
    },
  },
  icon: {
    margin: '0 5px',
  },
})

export default withStyles(styles)(withTheme()(Detail))
