import React from 'react'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Grow from '@material-ui/core/Grow'
import Typography from '@material-ui/core/Typography'

import {
  AuthorizationWrapper,
  CenterWrapper,
} from '../../core/'
import { InterestItem } from '../'

const Interest = (props) => {
  const {
    classes,
    data: {
      loading,
      employer,
      interests,
      error,
    },
    inPlace,
    theme: { transitions: { duration: { shortest } } },
  } = props

  return (
    <AuthorizationWrapper error={error}>
      <CenterWrapper>
        {!loading && (
          <Grid item xs={12} sm={8} md={6}>
            <Typography
              className={classes.title}
              variant="title"
            >
              Interests
            </Typography>
            <Grid container justify="center">
              {interests && interests.map((interest, index) => (
                <Grow
                  in={inPlace}
                  key={interest.id}
                  {...(inPlace && { timeout: (shortest * (index + 1)) })}
                >
                  <div>
                    <InterestItem
                      interest={interest}
                      employer={employer}
                    />
                  </div>
                </Grow>
              ))}
            </Grid>
          </Grid>
        )}
      </CenterWrapper>
    </AuthorizationWrapper>
  )
}

const styles = () => ({
  title: {
    textAlign: 'center',
  },
})

export default withStyles(styles)(withTheme()(Interest))
