import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'

import {
  AuthorizationWrapper,
  CenterWrapper,
} from '../../core/'
import { DetailItem } from '../'

const Detail = (props) => {
  const {
    classes,
    data: {
      loading,
      details,
      error,
    },
    inPlace,
    openSnackBar,
  } = props

  return (
    <AuthorizationWrapper error={error}>
      <CenterWrapper>
        <Grid item xs={12} md={3}>
          <Grid container justify="center">
            <Card className={classes.rounded}>
              <CardMedia
                className={classes.media}
                image={`${process.env.REACT_APP_URL}${process.env.REACT_APP_IMAGE}`}
              />
            </Card>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={8} md={6}>
          <Typography variant="title">
            Details
          </Typography>
          <List>
            {!loading && details && details.map((detail, index) => (
              <DetailItem
                detail={detail}
                index={index}
                inPlace={inPlace}
                key={detail.id}
                openSnackBar={openSnackBar}
              />
            ))}
          </List>
        </Grid>
      </CenterWrapper>
    </AuthorizationWrapper>
  )
}

const styles = () => ({
  rounded: {
    borderRadius: '50%',
    width: 200,
    height: 200,
    marginTop: 20,
  },
  media: {
    height: 200,
  },
})

export default withStyles(styles)(Detail)
