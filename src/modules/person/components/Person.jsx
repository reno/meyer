import React from 'react'

import {
  CenterWrapper,
  GetInTouch,
} from '../../core/'
import {
  Detail,
  Interest,
} from '../'

export default (props) => {
  const { inPlace } = props
  return (
    <React.Fragment>
      <Detail inPlace={inPlace} />
      <Interest inPlace={inPlace} />
      <CenterWrapper>
        <GetInTouch
          inPlace={inPlace}
          text="Contact me!"
        />
      </CenterWrapper>
    </React.Fragment>
  )
}
