import Detail from './containers/Detail'
import DetailItem from './components/DetailItem'
import Interest from './containers/Interest'
import InterestItem from './containers/InterestItem'
import Person from './components/Person'

export {
  Detail,
  DetailItem,
  Interest,
  InterestItem,
  Person,
}
