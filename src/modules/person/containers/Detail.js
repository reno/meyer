import { connect } from 'react-redux'
import {
  compose,
  graphql,
} from 'react-apollo'
import { getDetailsQuery } from '../queries'
import { openSnackBar } from '../../core/actions'
import Detail from '../components/Detail'


const mapDispatchToProps = dispatch => ({
  openSnackBar: message => dispatch(openSnackBar(message)),
})

export default compose(
  graphql(getDetailsQuery),
  connect(
    null,
    mapDispatchToProps,
  ),
)(Detail)
