import { connect } from 'react-redux'
import {
  compose,
  graphql,
} from 'react-apollo'
import { getInterestsQuery } from '../queries'
import { updateEmployerLikesMutation } from '../mutations'
import { openSnackBar } from '../../core/actions'
import InterestItem from '../components/InterestItem'

const mapDispatchToProps = dispatch => ({
  openSnackBar: message => dispatch(openSnackBar(message)),
})

export default compose(
  graphql(updateEmployerLikesMutation, {
    name: 'updateEmployerLikesMutation',
    options: {
      refetchQueries: [{
        query: getInterestsQuery,
      }],
    },
  }),
  connect(
    null,
    mapDispatchToProps,
  ),
)(InterestItem)

