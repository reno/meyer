import {
  compose,
  graphql,
} from 'react-apollo'
import { getInterestsQuery } from '../queries'
import Interest from '../components/Interest'

export default compose(
  graphql(getInterestsQuery),
)(Interest)
