import gql from 'graphql-tag'

export const getDetailsQuery = gql`
  query {
    details {
      id
      title
      link
      icon
      order
    }
  }
`

export const getInterestsQuery = gql`
  query {
    interests {
      id
      title
    }
    employer {
      id
      interestLikes
    }
  }
`
