import gql from 'graphql-tag'

// eslint-disable-next-line
export const getEmployerQuery = gql`
  query {
    employer {
      id
      institution
      job
      expiration
      sent
      createdAt
      intro
      motivation
      reference
      interestLikes
    }
    generals {
      id
      title
      text
    }
  }
`
