import { graphql } from 'react-apollo'
import { getEmployerQuery } from '../queries'
import Intro from '../components/Intro'

export default graphql(getEmployerQuery)(Intro)
