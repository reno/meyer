import React from 'react'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Grow from '@material-ui/core/Grow'
import Typography from '@material-ui/core/Typography'


import {
  AuthorizationWrapper,
  CenterWrapper,
  GetInTouch,
  HtmlParser,
} from '../../core/'

const Intro = (props) => {
  const {
    data: {
      loading,
      employer,
      error,
      generals,
    },
    inPlace,
    theme: { transitions: { duration: { shortest } } },
  } = props

  return (
    <AuthorizationWrapper error={error}>
      {!loading && (
        <React.Fragment>
          <CenterWrapper>
            <Grid container justify="center">
              <Typography variant="display2">
                {`Application for ${employer ? employer.job : 'Job'} @ ${employer ? employer.institution : 'Company'}`}
              </Typography>
            </Grid>
          </CenterWrapper>
          <CenterWrapper>
            <Grid item xs={12}>
              <Grow
                in={inPlace}
                {...((inPlace) && { timeout: shortest })}
              >
                <Grid container>
                  <Grid item xs={12} md={2} />
                  <Grid item xs={12} sm={8} md={6}>
                    <HtmlParser
                      content={employer && employer.intro}
                      fallback={generals && generals.find(el => el.title === 'intro') && generals.find(el => el.title === 'intro').text}
                    />
                  </Grid>
                  <Grid item xs={12} sm={4} />
                </Grid>
              </Grow>
              <Grow
                in={inPlace}
                {...((inPlace) && { timeout: 2 * shortest })}
              >
                <Grid container>
                  <Grid item xs={12} sm={4} />
                  <Grid item xs={12} sm={8} md={6}>
                    <HtmlParser
                      content={employer && employer.motivation}
                      fallback={generals && generals.find(el => el.title === 'motivation') && generals.find(el => el.title === 'motivation').text}
                    />
                  </Grid>
                </Grid>
              </Grow>
              <Grow
                in={inPlace}
                {...((inPlace) && { timeout: 3 * shortest })}
              >
                <Grid container justify="center">
                  <Grid item xs={12} sm={8} md={6}>
                    <HtmlParser
                      content={employer && employer.reference}
                      fallback={generals && generals.find(el => el.title === 'reference') && generals.find(el => el.title === 'reference').text}
                    />
                  </Grid>
                </Grid>
              </Grow>
            </Grid>
          </CenterWrapper>
          <CenterWrapper>
            <GetInTouch
              inPlace={inPlace}
              text="Get in touch now"
            />
          </CenterWrapper>
        </React.Fragment>
      )}
    </AuthorizationWrapper>
  )
}

const styles = () => ({
  title: {
    flex: 1,
  },
})

export default withStyles(styles)(withTheme()(Intro))
