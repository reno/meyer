import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'

import { getInterestsQuery } from '../queries'
import {
  createInterestMutation,
  deleteInterestMutation,
  updateInterestMutation,
  updateInterestOrderMutation,
} from '../mutations'
import DraggableList from '../components/DraggableList'

const componentConfig = {
  component: 'interests',
  formFields: [
    {
      id: 1,
      title: 'title',
      label: 'Title',
      type: 'text',
      required: true,
    },
  ],
  hasIcon: false,
  itemLabel: interest => interest.title,
  textFields: [],
}

export default compose(
  graphql(getInterestsQuery),
  graphql(createInterestMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getInterestsQuery,
      }],
    },
  }),
  graphql(deleteInterestMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getInterestsQuery,
      }],
    },
  }),
  graphql(updateInterestMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getInterestsQuery,
      }],
    },
  }),
  graphql(updateInterestOrderMutation, {
    name: 'updateOrderMutation',
    options: {
      refetchQueries: [{
        query: getInterestsQuery,
      }],
    },
  }),
)(inject(componentConfig)(DraggableList))
