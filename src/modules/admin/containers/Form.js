import { withFormik } from 'formik'
import moment from 'moment'

import Form from '../components/Form'

export default withFormik({
  mapPropsToValues: ({ formFields, element = {} }) => {
    const values = {}
    formFields.forEach((field) => {
      if (field.type === 'date') {
        values[field.title] = moment(element[field.title]).format('YYYY-MM-DD')
      } else {
        values[field.title] = element[field.title]
      }
    })
    return values
  },
  validate: (values, props) => {
    const errors = {}

    props.formFields.filter(f => f.required).forEach((field) => {
      if (!values[field.title]) {
        errors[field] = 'Required'
      }
    })

    return errors
  },
  handleSubmit: async (values, { props, setSubmitting, setErrors }) => {
    const {
      toggleModalAction,
      submitAction,
    } = props
    try {
      await submitAction(values)
      toggleModalAction()
    } catch (err) {
      setErrors({
        general: err.message,
      })
    }
    setSubmitting(false)
  },
})(Form)
