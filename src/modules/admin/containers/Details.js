import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'

import { getDetailsQuery } from '../queries'
import {
  createDetailMutation,
  deleteDetailMutation,
  updateDetailMutation,
  updateDetailOrderMutation,
} from '../mutations'
import DraggableList from '../components/DraggableList'

const componentConfig = {
  component: 'details',
  formFields: [
    {
      id: 1,
      title: 'title',
      label: 'Title',
      type: 'text',
    },
    {
      id: 2,
      title: 'link',
      label: 'Link',
      type: 'text',
    },
    {
      id: 3,
      title: 'icon',
      label: 'Icon',
      type: 'text',
    },
  ],
  hasIcon: true,
  itemLabel: detail => detail.title,
  textFields: [],
}

export default compose(
  graphql(getDetailsQuery),
  graphql(updateDetailOrderMutation, {
    name: 'updateOrderMutation',
    options: {
      refetchQueries: [{
        query: getDetailsQuery,
      }],
    },
  }),
  graphql(createDetailMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getDetailsQuery,
      }],
    },
  }),
  graphql(deleteDetailMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getDetailsQuery,
      }],
    },
  }),
  graphql(updateDetailMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getDetailsQuery,
      }],
    },
  }),
  graphql(updateDetailOrderMutation, {
    name: 'updateOrderMutation',
    options: {
      refetchQueries: [{
        query: getDetailsQuery,
      }],
    },
  }),
)(inject(componentConfig)(DraggableList))
