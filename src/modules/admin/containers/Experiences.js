import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'

import { getExperiencesQuery } from '../queries'
import {
  createExperienceMutation,
  deleteExperienceMutation,
  updateExperienceMutation,
  updateExperienceOrderMutation,
} from '../mutations'
import DraggableList from '../components/DraggableList'

const componentConfig = {
  component: 'experiences',
  formFields: [
    {
      id: 1,
      title: 'type',
      label: 'Type',
      type: 'text',
      select: true,
      options: ['PROFESSIONAL', 'EDUCATIONAL'],
    },
    {
      id: 2,
      title: 'period',
      label: 'Period',
      type: 'text',
    },
    {
      id: 3,
      title: 'title',
      label: 'Title',
      type: 'text',
    },
    {
      id: 4,
      title: 'institution',
      label: 'Institution',
      type: 'text',
    },
    {
      id: 5,
      title: 'link',
      label: 'Link',
      type: 'text',
    },
  ],
  hasIcon: false,
  itemLabel: experience => `${experience.title} @ ${experience.institution} (${experience.period})`,
  textFields: [
    {
      id: 1,
      title: 'description',
    },
  ],
}

export default compose(
  graphql(getExperiencesQuery),
  graphql(createExperienceMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getExperiencesQuery,
      }],
    },
  }),
  graphql(deleteExperienceMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getExperiencesQuery,
      }],
    },
  }),
  graphql(updateExperienceMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getExperiencesQuery,
      }],
    },
  }),
  graphql(updateExperienceOrderMutation, {
    name: 'updateOrderMutation',
    options: {
      refetchQueries: [{
        query: getExperiencesQuery,
      }],
    },
  }),
)(inject(componentConfig)(DraggableList))
