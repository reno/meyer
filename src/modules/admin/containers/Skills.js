import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'

import { getSkillsQuery } from '../queries'
import {
  createSkillMutation,
  deleteSkillMutation,
  updateSkillMutation,
  updateSkillOrderMutation,
} from '../mutations'
import DraggableList from '../components/DraggableList'

const componentConfig = {
  component: 'skills',
  formFields: [
    {
      id: 1,
      title: 'title',
      label: 'Title',
      type: 'text',
      required: true,
    },
    {
      id: 2,
      title: 'image',
      label: 'Image',
      type: 'text',
    },
  ],
  hasIcon: false,
  itemLabel: skill => skill.title,
  textFields: [
    {
      id: 1,
      title: 'description',
    },
  ],
}

export default compose(
  graphql(getSkillsQuery),
  graphql(createSkillMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getSkillsQuery,
      }],
    },
  }),
  graphql(deleteSkillMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getSkillsQuery,
      }],
    },
  }),
  graphql(updateSkillMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getSkillsQuery,
      }],
    },
  }),
  graphql(updateSkillOrderMutation, {
    name: 'updateOrderMutation',
    options: {
      refetchQueries: [{
        query: getSkillsQuery,
      }],
    },
  }),
)(inject(componentConfig)(DraggableList))
