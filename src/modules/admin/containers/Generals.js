import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'

import { getGeneralsQuery } from '../queries'
import {
  createGeneralMutation,
  deleteGeneralMutation,
  updateGeneralMutation,
} from '../mutations'
import List from '../components/List'

const componentConfig = {
  component: 'generals',
  formFields: [
    {
      id: 1,
      title: 'title',
      label: 'Title',
      type: 'text',
      required: true,
    },
  ],
  hasIcon: false,
  itemLabel: general => general.title,
  textFields: [
    {
      id: 1,
      title: 'text',
    },
  ],
}

export default compose(
  graphql(getGeneralsQuery),
  graphql(createGeneralMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getGeneralsQuery,
      }],
    },
  }),
  graphql(deleteGeneralMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getGeneralsQuery,
      }],
    },
  }),
  graphql(updateGeneralMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getGeneralsQuery,
      }],
    },
  }),
)(inject(componentConfig)(List))
