import React from 'react'
import {
  compose,
  graphql,
} from 'react-apollo'
import { compose as inject } from 'react-compose'
import moment from 'moment'

import Icon from '@material-ui/core/Icon'

import { getEmployersQuery } from '../queries'
import {
  createEmployerMutation,
  getEmployerTokenMutation,
  deleteEmployerMutation,
  updateEmployerMutation,
} from '../mutations'
import List from '../components/List'

const iconMap = {
  PREPARED: 'note_add',
  SENT: 'mail',
  DISCUSSING: 'forum',
  OFFERED: 'thumb_up_alt',
  DECLINED: 'thumb_down_alt',
}

const iconColorMap = {
  PREPARED: 'disabled',
  SENT: 'action',
  DISCUSSING: 'primary',
  OFFERED: 'secondary',
  DECLINED: 'error',
}

const componentConfig = {
  component: 'employers',
  formFields: [
    {
      id: 1,
      title: 'institution',
      label: 'Institution',
      type: 'text',
    },
    {
      id: 2,
      title: 'job',
      label: 'Job',
      type: 'text',
    },
    {
      id: 3,
      title: 'expiration',
      label: 'Expiration',
      type: 'date',
    },
    {
      id: 4,
      title: 'status',
      label: 'Status',
      type: 'text',
      select: true,
      options: ['PREPARED', 'SENT', 'DISCUSSING', 'OFFERED', 'DECLINED'],
    },
  ],
  hasIcon: false,
  itemLabel: employer => (
    <React.Fragment>
      {employer.job} @ {employer.institution} ({moment(employer.expiration).format('DD.MM.YYYY')})
      {employer.status && (
        <Icon color={iconColorMap[employer.status]} style={{ fontSize: 16, marginLeft: 5 }}>{iconMap[employer.status]}</Icon>
      )}
    </React.Fragment>
  ),
  textFields: [
    {
      id: 1,
      title: 'intro',
    },
    {
      id: 2,
      title: 'motivation',
    },
    {
      id: 3,
      title: 'reference',
    },
  ],
  tokenAction: true,
}

export default compose(
  graphql(getEmployersQuery),
  graphql(createEmployerMutation, {
    name: 'createMutation',
    options: {
      refetchQueries: [{
        query: getEmployersQuery,
      }],
    },
  }),
  graphql(deleteEmployerMutation, {
    name: 'deleteMutation',
    options: {
      refetchQueries: [{
        query: getEmployersQuery,
      }],
    },
  }),
  graphql(getEmployerTokenMutation, {
    name: 'getEmployerTokenMutation',
  }),
  graphql(updateEmployerMutation, {
    name: 'updateMutation',
    options: {
      refetchQueries: [{
        query: getEmployersQuery,
      }],
    },
  }),
)(inject(componentConfig)(List))
