import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import {
  DefaultEmployer,
  EmployerCreate,
  EmployerList,
} from '../'

const Employers = (props) => {
  const {
    classes,
    createEmployerMutation,
  } = props

  return (
    <Paper className={classes.paper}>
      <Toolbar>
        <Typography
          className={classes.title}
          variant="title"
        >
          Employers
        </Typography>
        <EmployerCreate createEmployerMutation={createEmployerMutation} />
      </Toolbar>
      <EmployerList />
      <DefaultEmployer />
    </Paper>
  )
}

const styles = {
  paper: {
    margin: '5px',
  },
  title: {
    flex: 1,
  },
}

export default withStyles(styles)(Employers)
