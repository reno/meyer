import React from 'react'
import copy from 'copy-to-clipboard'

import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'

import {
  Edit,
  TextEditor,
} from '../'

export default class Item extends React.Component {
  constructor() {
    super()

    this.state = {
      open: false,
    }

    this.toggleModal = this.toggleModal.bind(this)
    this.deleteMutation = this.deleteMutation.bind(this)
    this.updateMutation = this.updateMutation.bind(this)
    this.updateFieldMutation = this.updateFieldMutation.bind(this)
  }

  toggleModal() {
    this.setState({
      open: !this.state.open,
    })
  }

  deleteMutation(event) {
    event.preventDefault()
    const {
      element,
      deleteMutation,
    } = this.props

    deleteMutation({
      variables: {
        id: element.id,
      },
    })
  }

  updateMutation(updated) {
    const {
      element,
      updateMutation,
    } = this.props

    updateMutation({
      variables: {
        id: element.id,
        ...updated,
      },
    })
  }

  updateFieldMutation(fieldName) {
    return ((value) => {
      const {
        element,
        updateMutation,
      } = this.props

      updateMutation({
        variables: {
          id: element.id,
          [fieldName]: value,
        },
      })
    })
  }

  render() {
    const {
      deleteMutation,
      element,
      formFields = [],
      getEmployerTokenMutation,
      hasIcon,
      itemLabel,
      textFields = [],
      tokenAction,
    } = this.props

    const { open } = this.state

    return (
      <React.Fragment>
        {element ? (
          <React.Fragment>
            <ListItem
              button
              onClick={this.toggleModal}
            >
              {hasIcon && (
                <ListItemIcon>
                  <Icon>
                    {element.icon}
                  </Icon>
                </ListItemIcon>
              )}
              <ListItemText primary={itemLabel(element)} />
              <ListItemSecondaryAction>
                <React.Fragment>
                  {tokenAction && (
                    <Button
                      onClick={async () => {
                        const res = await getEmployerTokenMutation({
                          variables: {
                            id: element.id,
                            expiration: element.expiration,
                          },
                        })
                        copy(res.data.getEmployerToken)
                      }}
                    >
                      get token
                    </Button>
                  )}
                </React.Fragment>
                {textFields.map(field => (
                  <TextEditor
                    key={`textField-${field.id}`}
                    content={element[field.title]}
                    title={field.title}
                    onSave={this.updateFieldMutation(field.title)}
                  />
                ))}
                <IconButton
                  onClick={() => deleteMutation({
                    variables: { id: element.id },
                  })}
                >
                  <Icon>delete</Icon>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
            <Edit
              element={element}
              formFields={formFields}
              open={open}
              toggleModal={this.toggleModal}
              updateMutation={this.updateMutation}
            />
          </React.Fragment>
        ) : null}
      </React.Fragment>
    )
  }
}
