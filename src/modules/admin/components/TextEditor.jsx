import React from 'react'
import {
  convertFromRaw,
  convertToRaw,
  EditorState,
} from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

const toolbarOptions = {
  options: ['inline', 'blockType', 'list', 'link'],
  inline: {
    options: ['bold', 'italic', 'superscript', 'subscript'],
  },
  blockType: {
    options: ['Normal', 'H2'],
  },
  list: {
    options: ['unordered'],
  },
  link: {
    defaultTargetOption: '_blank',
    options: ['link', 'unlink'],
  },
}

export default class TextEditor extends React.Component {
  constructor(props) {
    super(props)

    const { content } = props

    let editorState = EditorState.createEmpty()

    if (content) {
      editorState = EditorState.createWithContent(convertFromRaw(JSON.parse(content)))
    }

    this.state = {
      editorState,
      open: false,
    }

    this.onEditorStateChange = this.onEditorStateChange.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.onSave = this.onSave.bind(this)
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
    })
  }

  onSave() {
    const { onSave } = this.props
    const { editorState } = this.state

    onSave(JSON.stringify(convertToRaw(editorState.getCurrentContent())))
    this.toggleModal()
  }

  toggleModal() {
    this.setState({
      open: !this.state.open,
    })
  }

  render() {
    const { title = 'new wysiwyg' } = this.props

    const { editorState } = this.state

    return (
      <React.Fragment>
        <Button
          onClick={this.toggleModal}
        >
          {title}
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.toggleModal}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <Editor
              editorState={editorState}
              onEditorStateChange={this.onEditorStateChange}
              toolbar={toolbarOptions}
              editorStyle={{ fontFamily: 'Barlow Semi Condensed' }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.toggleModal}>
              Cancel
            </Button>
            <Button
              color="primary"
              onClick={this.onSave}
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}

