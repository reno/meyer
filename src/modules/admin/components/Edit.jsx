import React from 'react'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'

import { Form } from '../'

export default (props) => {
  const {
    element,
    formFields,
    open,
    toggleModal,
    updateMutation,
  } = props

  return (
    <React.Fragment>
      <Dialog
        aria-labelledby="form-dialog-title"
        open={open}
        onClose={toggleModal}
      >
        <DialogTitle id="form-dialog-title">Edit General</DialogTitle>
        <Form
          element={element}
          formFields={formFields}
          submitAction={updateMutation}
          submitActionTitle="Edit"
          toggleModalAction={toggleModal}
        />
      </Dialog>
    </React.Fragment>
  )
}

