import React from 'react'
import { Container, Draggable } from 'react-smooth-dnd'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import List from '@material-ui/core/List'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import {
  Create,
  Item,
} from '../'

class DraggableList extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if ((!props.data.loading) && (state.order.length < props.data[props.component].length)) {
      const order = props.data[props.component].map(element => element.id)
      return ({
        order,
      })
    }
    return null
  }

  constructor() {
    super()

    this.state = {
      order: [],
      orderChanged: false,
    }

    this.getChildPayload = this.getChildPayload.bind(this)
    this.saveOrder = this.saveOrder.bind(this)
    this.updateOrder = this.updateOrder.bind(this)
  }

  getChildPayload(index) {
    return this.state.order[index]
  }

  updateOrder(args) {
    const {
      removedIndex,
      addedIndex,
      payload,
    } = args

    const { order } = this.state

    order.splice(removedIndex, 1)
    order.splice(addedIndex, 0, payload)

    this.setState({
      order,
      orderChanged: true,
    })
  }

  saveOrder() {
    const { updateOrderMutation } = this.props
    const { order } = this.state

    updateOrderMutation({
      variables: { elements: order },
    })

    this.setState({
      orderChanged: false,
    })
  }

  render() {
    const {
      classes,
      component,
      createMutation,
      data,
      deleteMutation,
      formFields,
      hasIcon,
      itemLabel,
      textFields,
      updateMutation,
    } = this.props

    const {
      order,
      orderChanged,
    } = this.state

    return (
      <Paper className={classes.paper}>
        <Toolbar>
          <Typography
            className={classes.title}
            variant="title"
          >
            {component}
          </Typography>
          <Button
            className={classes.button}
            disabled={!orderChanged}
            onClick={this.saveOrder}
            variant="outlined"
          >
            Save Order
            <Icon>save</Icon>
          </Button>
          <Create
            createMutation={createMutation}
            component={component}
            formFields={formFields}
          />
        </Toolbar>
        {!data.loading && (
          <List>
            <Container
              onDrop={this.updateOrder}
              getChildPayload={this.getChildPayload}
            >
              {order && (order.length > 0) && order.filter(item => data[component].find(el => el.id === item)).map(element => (
                <Draggable
                  key={element}
                >
                  <Item
                    key={element}
                    deleteMutation={deleteMutation}
                    element={data[component].find(el => el.id === element)}
                    formFields={formFields}
                    hasIcon={hasIcon}
                    itemLabel={itemLabel}
                    textFields={textFields}
                    updateMutation={updateMutation}
                  />
                </Draggable>
              ))}
            </Container>
          </List>
        )}
      </Paper>
    )
  }
}

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  paper: {
    margin: '5px',
  },
  title: {
    flex: 1,
  },
})

export default withStyles(styles)(DraggableList)
