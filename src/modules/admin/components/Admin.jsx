import React from 'react'
import SwipeableViews from 'react-swipeable-views'

import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import {
  Details,
  Employers,
  Experiences,
  Generals,
  Interests,
  Skills,
} from '../'

class Admin extends React.Component {
  constructor() {
    super()

    this.state = {
      value: 0,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleChangeIndex = this.handleChangeIndex.bind(this)
  }

  handleChange(event, value) {
    this.setState({ value })
  }

  handleChangeIndex(index) {
    this.setState({ value: index })
  }

  render() {
    const { classes } = this.props

    return (
      <React.Fragment>
        <AppBar position="static" color="default" className={classes.tabs}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
          >
            <Tab label="Employers" />
            <Tab label="Details" />
            <Tab label="Interests" />
            <Tab label="Skills" />
            <Tab label="Experiences" />
            <Tab label="Generals" />
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis="x"
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
        >
          <Employers />
          <Details />
          <Interests />
          <Skills />
          <Experiences />
          <Generals />
        </SwipeableViews>
      </React.Fragment>
    )
  }
}

const styles = {
  tabs: {
    marginBottom: 20,
  },
}

export default withStyles(styles)(Admin)
