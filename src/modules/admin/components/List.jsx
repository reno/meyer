import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import {
  Create,
  Item,
} from '../'

const AdminList = (props) => {
  const {
    classes,
    component,
    createMutation,
    data,
    deleteMutation,
    formFields,
    getEmployerTokenMutation,
    hasIcon,
    itemLabel,
    textFields,
    tokenAction,
    updateMutation,
  } = props

  return (
    <Paper className={classes.paper}>
      <Toolbar>
        <Typography
          className={classes.title}
          variant="title"
        >
          {component}
        </Typography>
        <Create
          createMutation={createMutation}
          component={component}
          formFields={formFields}
        />
      </Toolbar>
      {data && !data.loading && (
        <List>
          {data[component].map(element => (
            <Item
              key={element.id}
              deleteMutation={deleteMutation}
              element={element}
              formFields={formFields}
              getEmployerTokenMutation={getEmployerTokenMutation}
              hasIcon={hasIcon}
              itemLabel={itemLabel}
              textFields={textFields}
              tokenAction={tokenAction}
              updateMutation={updateMutation}
            />
          ))}
        </List>
      )}
    </Paper>
  )
}

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  paper: {
    margin: '5px',
  },
  title: {
    flex: 1,
  },
})

export default withStyles(styles)(AdminList)
