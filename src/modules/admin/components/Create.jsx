import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import Icon from '@material-ui/core/Icon'

import { Form } from '../'

class Create extends React.Component {
  constructor() {
    super()

    this.state = {
      open: false,
    }

    this.toggleModal = this.toggleModal.bind(this)
    this.createMutation = this.createMutation.bind(this)
  }

  toggleModal() {
    this.setState({
      open: !this.state.open,
    })
  }

  createMutation(newElement) {
    const { createMutation } = this.props
    createMutation({
      variables: {
        ...newElement,
      },
    })
  }

  render() {
    const {
      component,
      element,
      formFields,
    } = this.props

    return (
      <React.Fragment>
        <Button
          color="primary"
          mini
          variant="outlined"
          onClick={this.toggleModal}
        >
          New {component}
          <Icon>add_circle_outline</Icon>
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.toggleModal}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Create new {component}</DialogTitle>

          <Form
            element={element}
            formFields={formFields}
            submitAction={this.createMutation}
            submitActionTitle="Create"
            toggleModalAction={this.toggleModal}
          />
        </Dialog>
      </React.Fragment>
    )
  }
}

const styles = {}

export default withStyles(styles)(Create)

