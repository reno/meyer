import React from 'react'

import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'

export default (props) => {
  const {
    errors,
    formFields,
    handleChange,
    handleSubmit,
    isSubmitting,
    submitActionTitle,
    toggleModalAction,
    touched,
    values,
  } = props

  return (
    <form onSubmit={handleSubmit}>
      <DialogContent>
        {formFields.map((field, index) => (
          <React.Fragment key={`formField-${field.id}`}>
            {field.type === 'checkbox' ? (
              <FormControlLabel
                control={
                  <Checkbox
                    checked={values[field.title]}
                    color="primary"
                    name={field.title}
                    onChange={handleChange}
                  />
                }
                label={field.label}
              />
            ) : (
              <TextField
                autoFocus={index === 0}
                error={touched[field.title] && !!errors[field.title]}
                fullWidth
                helperText={touched[field.title] && errors[field.title]}
                id={field.title}
                label={field.label}
                margin="dense"
                multiline={field.multiline}
                name={field.title}
                onChange={handleChange}
                rowsMax="4"
                select={field.select}
                type={field.type}
                value={values[field.title]}
              >
                {field.select && field.options.map(option => (
                  <MenuItem
                    key={option}
                    value={option}
                  >
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            )}
          </React.Fragment>
        ))}
      </DialogContent>
      {errors.general && (
        <DialogContent>
          {errors.general}
        </DialogContent>
      )}
      <DialogActions>
        <Button onClick={toggleModalAction}>
          Cancel
        </Button>
        <Button type="submit" disabled={isSubmitting} color="primary">
          {submitActionTitle}
        </Button>
      </DialogActions>
    </form>
  )
}
