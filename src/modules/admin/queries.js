import gql from 'graphql-tag'

export const getGeneralsQuery = gql`
  query {
    generals {
      id
      title
      text
    }
  }
`

export const getEmployersQuery = gql`
  query {
    employers {
      id
      institution
      job
      expiration
      status
      createdAt
      intro
      motivation
      reference
    }
  }
`

export const getDetailsQuery = gql`
  query {
    details {
      id
      title
      link
      icon
    }
  }
`

export const getInterestsQuery = gql`
  query {
    interests {
      id
      title
    }
  }
`

export const getSkillsQuery = gql`
  query {
    skills {
      id
      title
      image
      description
    }
  }
`


export const getExperiencesQuery = gql`
  query {
    experiences {
      id
      type
      period
      title
      institution
      link
      description
    }
  }
`
