import gql from 'graphql-tag'

// General
export const createGeneralMutation = gql`
  mutation createGeneralMutation($title: String!){
    createGeneral(title: $title) {
      id
    }
  }
`

export const deleteGeneralMutation = gql`
  mutation deleteGeneralMutation($id: ID!){
    deleteGeneral(id: $id)
  }
`

export const updateGeneralMutation = gql`
  mutation updateGeneralMutation(
    $id: ID!,
    $title: String,
    $text: String,
  ){
    updateGeneral(id: $id, title: $title, text: $text){
      id
    }
  }
`

// Employer
export const createEmployerMutation = gql`
  mutation createEmployerMutation(
    $institution: String!,
    $job: String!,
    $expiration: String!,
    $status: String,
  ){
    createEmployer(
      institution: $institution,
      job: $job,
      expiration: $expiration,
      status: $status,
    ){
      id
    }
  }
`

export const deleteEmployerMutation = gql`
  mutation deleteEmployerMutation($id: ID!){
    deleteEmployer(id: $id)
  }
`

export const getEmployerTokenMutation = gql`
  mutation getEmployerTokenMutation($id: ID!, $expiration: String!){
    getEmployerToken(id: $id, expiration: $expiration)
  }
`

export const updateEmployerMutation = gql`
  mutation updateEmployerMutation(
    $id: ID!,
    $institution: String,
    $job: String,
    $expiration: String,
    $status: String,
    $intro: String,
    $motivation: String,
    $reference: String
  ){
    updateEmployer(id: $id,
      institution: $institution,
      job: $job,
      expiration: $expiration,
      status: $status,
      intro: $intro,
      motivation: $motivation,
      reference: $reference
    ){
      id
    }
  }
`

// Detail
export const createDetailMutation = gql`
  mutation createDetailMutation(
    $title: String!,
    $link: String,
    $icon: String!,
  ){
    createDetail(
      title: $title,
      link: $link,
      icon: $icon,
    ){
      id
    }
  }
`

export const deleteDetailMutation = gql`
  mutation deleteDetailMutation($id: ID!){
    deleteDetail(id: $id)
  }
`

export const updateDetailMutation = gql`
  mutation updateDetailMutation(
    $id: ID!,
    $title: String!,
    $link: String,
    $icon: String!,
  ){
    updateDetail(
      id: $id,
      title: $title,
      link: $link,
      icon: $icon,
    ){
      id
    }
  }
`

export const updateDetailOrderMutation = gql`
  mutation updateDetailOrderMutation($elements: [ID!]!){
    updateDetailOrder(elements: $elements)
  }
`

// Interest
export const createInterestMutation = gql`
  mutation createInterestMutation($title: String!){
    createInterest(title: $title){
      id
    }
  }
`

export const deleteInterestMutation = gql`
  mutation deleteInterestMutation($id: ID!){
    deleteInterest(id: $id)
  }
`

export const updateInterestMutation = gql`
  mutation updateInterestMutation($id: ID!, $title: String!){
    updateInterest(id: $id, title: $title){
      id
    }
  }
`

export const updateInterestOrderMutation = gql`
  mutation updateInterestOrderMutation($elements: [ID!]!){
    updateInterestOrder(elements: $elements)
  }
`

// Skill
export const createSkillMutation = gql`
  mutation createSkillMutation($title: String!, $image: String){
    createSkill(title: $title, image: $image){
      id
    }
  }
`

export const deleteSkillMutation = gql`
  mutation deleteSkillMutation($id: ID!){
    deleteSkill(id: $id)
  }
`

export const updateSkillMutation = gql`
  mutation updateSkillMutation(
    $id: ID!,
    $title: String,
    $image: String,
    $description: String,
  ){
    updateSkill(id: $id, title: $title, image: $image, description: $description){
      id
    }
  }
`

export const updateSkillOrderMutation = gql`
  mutation updateSkillOrderMutation($elements: [ID!]!){
    updateSkillOrder(elements: $elements)
  }
`

// Experience
export const createExperienceMutation = gql`
  mutation createExperienceMutation($type: String!,
    $period: String!,
    $title: String!,
    $institution: String!,
    $link: String,
  ){
    createExperience(
      type: $type,
      period: $period,
      title: $title,
      institution: $institution,
      link: $link,
    ){
      id
    }
  }
`

export const deleteExperienceMutation = gql`
  mutation deleteExperienceMutation($id: ID!){
    deleteExperience(id: $id)
  }
`

export const updateExperienceMutation = gql`
  mutation updateExperienceMutation(
    $id: ID!,
    $type: String,
    $period: String,
    $title: String,
    $institution: String,
    $link: String,
    $description: String,
  ){
    updateExperience(
      id: $id,
      type: $type,
      period: $period,
      title: $title,
      institution: $institution,
      link: $link,
      description: $description,
    ){
      id
    }
  }
`

export const updateExperienceOrderMutation = gql`
  mutation updateExperienceOrderMutation($elements: [ID!]!){
    updateExperienceOrder(elements: $elements)
  }
`
