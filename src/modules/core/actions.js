import * as types from '../../utils/actionTypes'


// NavIndex
const setNavIndexAction = index => ({
  type: types.SET_NAV_INDEX,
  payload: index,
})

export const setNavIndex = index =>
  dispatch => dispatch(setNavIndexAction(index))


// SnackBar
const openSnackBarAction = message => ({
  type: types.OPEN_SNACKBAR,
  payload: message,
})

export const openSnackBar = message =>
  dispatch => dispatch(openSnackBarAction(message))


// Error
const setErrorAction = error => ({
  type: types.SET_ERROR,
  payload: error,
})

export const setError = error =>
  dispatch => dispatch(setErrorAction(error))
