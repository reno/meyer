import React from 'react'
import SwipeableViews from 'react-swipeable-views'

import { withStyles } from '@material-ui/core/styles'
import { Intro } from '../../intro'
import { Person } from '../../person'
import { Skill } from '../../skill'
import { Experience } from '../../experience'

class Main extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inPlace: props.navIndex,
    }

    this.updateInPlace = this.updateInPlace.bind(this)
  }

  updateInPlace() {
    this.setState({
      inPlace: this.props.navIndex,
    })
  }

  render() {
    const {
      classes,
      navIndex,
    } = this.props

    const { inPlace } = this.state

    return (
      <div className={classes.main}>
        <SwipeableViews
          axis="x"
          index={navIndex}
          onTransitionEnd={this.updateInPlace}
        >
          <Intro inPlace={(inPlace === 0)} />
          <Person inPlace={(inPlace === 1)} />
          <Skill inPlace={(inPlace === 2)} />
          <Experience inPlace={(inPlace === 3)} />
        </SwipeableViews>
      </div>
    )
  }
}

const styles = () => ({
  main: {
    marginBottom: 40,
  },
})

export default withStyles(styles)(Main)
