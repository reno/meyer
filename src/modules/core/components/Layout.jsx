import React from 'react'

import {
  Header,
  Main,
  Notification,
} from '../'

export default () => (
  <React.Fragment>
    <Header />
    <Main />
    <Notification />
  </React.Fragment>
)
