import React from 'react'
import draftToHtml from 'draftjs-to-html'

import { withStyles } from '@material-ui/core/styles'

const HtmlParser = (props) => {
  const {
    classes,
    content,
    fallback = false,
  } = props

  let value
  let contentObject

  const checkNotEmpty = obj => (('blocks' in obj) && (obj.blocks.length > 1 || obj.blocks[0].text))

  if (content) {
    contentObject = JSON.parse(content)
    value = checkNotEmpty(contentObject) && contentObject
  }

  if (!value && fallback) {
    contentObject = JSON.parse(fallback)
    value = checkNotEmpty(contentObject) && contentObject
  }

  return (
    <div
      className={classes.html}
      {...(value && { dangerouslySetInnerHTML: { __html: draftToHtml(value) } })}
    />
  )
}

const styles = theme => ({
  html: {
    fontFamily: theme.typography.fontFamily,
    '& h2': {
      fontWeight: 300,
    },
    '& ul': {
      textAlign: 'left',
    },
    '& a': {
      color: theme.palette.secondary.dark,
      textDecoration: 'none',
    },
  },
})

export default withStyles(styles)(HtmlParser)
