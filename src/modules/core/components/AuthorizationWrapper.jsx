import React from 'react'

import { Redirect } from 'react-router-dom'

class AuthorizationWrapper extends React.Component {
  static getDerivedStateFromProps(props) {
    const {
      error,
      setError,
    } = props

    if (error) {
      setError(error.message)
      return ({
        error: error.message,
      })
    }
    return null
  }

  constructor(props) {
    super(props)

    const { error } = props

    this.state = {
      error,
    }
  }

  render() {
    const { children } = this.props

    const { error } = this.state

    return (
      <React.Fragment>
        {error ? (
          <Redirect to="/unauthorized" />
        ) : (children)}
      </React.Fragment>
    )
  }
}

export default AuthorizationWrapper
