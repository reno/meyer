import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Icon from '@material-ui/core/Icon'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'

import blueGrey from '@material-ui/core/colors/blueGrey'

import { CenterWrapper } from '../'

const Header = (props) => {
  const {
    classes,
    navIndex,
    setNavIndex,
  } = props

  return (
    <div className={classes.header}>
      <CenterWrapper marginTop={0}>
        <Grid item xs={12}>
          <Grid
            className={classes.header}
            container
            alignItems="flex-end"
            justify="space-around"
          >
            <Grid item xs={12} md={4}>
              <Typography
                className={classes.title}
                variant="display4"
              >
                {process.env.REACT_APP_TITLE.split(' ').map(word => (
                  <span
                    key={word}
                  >
                    {word}
                  </span>
                ))}
              </Typography>
            </Grid>
            <Grid item xs={12} md={8}>
              <Tabs
                className={classes.tabs}
                value={navIndex}
                onChange={(event, index) => setNavIndex(index)}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
              >
                <Tab
                  classes={{
                    root: classes.tab,
                    labelContainer: classes.tabLabel,
                  }}
                  icon={<Icon style={{ fontSize: 35 }}>drafts</Icon>}
                  label="Intro"
                />
                <Tab
                  classes={{
                    root: classes.tab,
                    labelContainer: classes.tabLabel,
                  }}
                  icon={<Icon style={{ fontSize: 35 }}>person</Icon>}
                  label="Person"
                />
                <Tab
                  classes={{
                    root: classes.tab,
                    labelContainer: classes.tabLabel,
                  }}
                  icon={<Icon style={{ fontSize: 35 }}>build</Icon>}
                  label="Skills"
                />
                <Tab
                  classes={{
                    root: classes.tab,
                    labelContainer: classes.tabLabel,
                  }}
                  icon={<Icon style={{ fontSize: 35 }}>work</Icon>}
                  label="Experiences"
                />
              </Tabs>
            </Grid>
          </Grid>
        </Grid>
      </CenterWrapper>
    </div>
  )
}

const styles = theme => ({
  header: {
  },
  title: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '4rem',
      marginBottom: 20,
    },
    '& span': {
      marginRight: 20,
      [theme.breakpoints.up('md')]: {
        display: 'block',
      },
    },
  },
  tab: {
    borderRadius: 3,
    transition: `background-color ${theme.transitions.duration.short}ms ${theme.transitions.easing.easeIn}`,
    [theme.breakpoints.up('md')]: {
      minWidth: 120,
    },
    '&:hover': {
      backgroundColor: blueGrey[50],
      '& $tabLabel': {
        opacity: 1,
      },
    },
  },
  tabLabel: {
    opacity: 0,
    transition: `opacity ${theme.transitions.duration.short}ms ${theme.transitions.easing.easeIn}`,
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
})

export default withStyles(styles)(Header)
