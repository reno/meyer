import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

const CenterWrapper = (props) => {
  const {
    children,
    classes,
    marginTop = 50,
  } = props


  return (
    <Grid
      container
      justify="center"
      style={{ marginTop }}
    >
      <Grid
        className={classes.root}
        container
        justify="center"
        spacing={24}
      >
        {children}
      </Grid>
    </Grid>
  )
}

const styles = theme => ({
  root: {
    maxWidth: theme.breakpoints.width('md'),
    margin: '5px 0',
  },
})

export default withStyles(styles)(CenterWrapper)
