import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import {
  CenterWrapper,
  GetInTouch,
} from '../'

const Unauthorized = (props) => {
  const {
    classes,
    error,
  } = props

  return (
    <Grid
      className={classes.root}
      container
      justify="center"
      alignContent="center"
    >
      <div>
        <Typography
          align="center"
          className={classes.title}
          variant="display4"
        >
          Unauthorized
        </Typography>
        <Typography
          align="center"
          color="secondary"
          variant="body1"
        >
          {error}
        </Typography>
        <Typography
          align="center"
          variant="body1"
        >
          Please ask for a new authentication link.
        </Typography>
      </div>
      <CenterWrapper>
        <GetInTouch
          inPlace
          text="Request access"
        />
      </CenterWrapper>
    </Grid>
  )
}

const styles = theme => ({
  root: {
    minHeight: `calc(${-2 * theme.spacing.unit}px + 100vh)`,
  },
  title: {
    [theme.breakpoints.down('xs')]: {
      fontSize: '4rem',
      marginBottom: 20,
    },
  },
})

export default withStyles(styles)(Unauthorized)
