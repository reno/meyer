import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import Fade from '@material-ui/core/Fade'

const GetInTouch = (props) => {
  const {
    classes,
    inPlace,
    text,
  } = props

  return (
    <Fade
      in={inPlace}
    >
      <Button
        color="secondary"
        href={`mailto:${process.env.REACT_APP_EMAIL}`}
        size="large"
        variant="outlined"
      >
        <Icon className={classes.icon}>email</Icon>
        {text}
      </Button>
    </Fade>
  )
}

const styles = () => ({
  icon: {
    marginRight: 15,
  },
})

export default withStyles(styles)(GetInTouch)
