import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import decode from 'jwt-decode'

const isAuthenticated = (setError) => {
  try {
    const token = localStorage.getItem('token')
    const { exp } = decode(token)
    if (Date.now() / 1000 > exp) {
      setError('Token expired')
      return false
    }
  } catch (err) {
    setError(err.message)
    return false
  }
  return true
}

export default ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (isAuthenticated(rest.setError) ? (
        <Component {...props} />
      ) : (
        <Redirect to="/unauthorized" />
      ))
    }
  />
)
