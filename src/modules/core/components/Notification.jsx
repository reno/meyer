import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'

class Notification extends React.Component {
  static getDerivedStateFromProps(props, state) {
    const {
      snackBar: { message },
    } = props

    if (message !== state.message) {
      return ({
        open: true,
        message,
      })
    }
    return null
  }

  constructor() {
    super()

    this.state = {
      open: false,
      message: '',
    }

    this.handleClose = this.handleClose.bind(this)
  }

  handleClose(event, reason) {
    if (reason === 'clickaway') {
      return
    }

    this.setState({
      open: false,
      message: '',
    })
  }

  render() {
    const { classes } = this.props

    const {
      open,
      message,
    } = this.state

    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        autoHideDuration={3000}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        onClose={this.handleClose}
        onExited={this.handleClose}
        open={open}
      >
        <SnackbarContent
          className={classes.root}
          message={
            <span id="client-snackbar" className={classes.message}>
              {message}
            </span>
          }
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <Icon>close</Icon>
            </IconButton>,
          ]}
        />
      </Snackbar>
    )
  }
}

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.secondary.main,
  },
})

export default withStyles(styles)(Notification)
