import { createSelector } from 'reselect'

export const getNavIndex = createSelector(
  [
    state => state.core.navIndex,
  ],
  navIndex => navIndex,
)

export const getSnackBar = createSelector(
  [
    state => state.core.snackBar,
  ],
  snackBar => snackBar,
)

export const getError = createSelector(
  [
    state => state.core.error,
  ],
  error => error,
)
