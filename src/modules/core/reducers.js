import { combineReducers } from 'redux'
import * as types from '../../utils/actionTypes'
import { createReducer } from '../../utils/helpers'

const initialState = {
  navIndex: 0,
  snackBar: { message: '' },
  error: '',
}

// NavIndex
const setNavIndex = (state, payload) => payload

export const navIndex = createReducer(initialState.navIndex, {
  [types.SET_NAV_INDEX]: setNavIndex,
})

// SnackBar
const openSnackBar = (state, payload) => ({
  message: payload,
})

export const snackBar = createReducer(initialState.snackBar, {
  [types.OPEN_SNACKBAR]: openSnackBar,
})

// Error
const setError = (state, payload) => payload

export const error = createReducer(initialState.error, {
  [types.SET_ERROR]: setError,
})

// Default Export
export default combineReducers({
  navIndex,
  snackBar,
  error,
})
