import { connect } from 'react-redux'
import { setError } from '../actions'
import AuthorizationWrapper from '../components/AuthorizationWrapper'

const mapDispatchToProps = dispatch => ({
  setError: error => dispatch(setError(error)),
})

export default connect(
  null,
  mapDispatchToProps,
)(AuthorizationWrapper)
