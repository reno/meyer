import { connect } from 'react-redux'
import { setError } from '../actions'
import PrivateRoute from '../components/PrivateRoute'

const mapDispatchToProps = dispatch => ({
  setError: error => dispatch(setError(error)),
})

export default connect(
  null,
  mapDispatchToProps,
)(PrivateRoute)
