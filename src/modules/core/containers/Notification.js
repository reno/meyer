import { connect } from 'react-redux'

import { getSnackBar } from '../selectors'
import Notification from '../components/Notification'

const mapStateToProps = state => ({
  snackBar: getSnackBar(state),
})

export default connect(
  mapStateToProps,
  null,
)(Notification)
