import { connect } from 'react-redux'
import { getNavIndex } from '../selectors'
import Main from '../components/Main'

const mapStateToProps = state => ({
  navIndex: getNavIndex(state),
})

export default connect(
  mapStateToProps,
  null,
)(Main)
