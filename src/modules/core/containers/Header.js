import { connect } from 'react-redux'
import { getNavIndex } from '../selectors'
import { setNavIndex } from '../actions'
import Header from '../components/Header'

const mapStateToProps = state => ({
  navIndex: getNavIndex(state),
})

const mapDispatchToProps = dispatch => ({
  setNavIndex: index => dispatch(setNavIndex(index)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header)
