import { connect } from 'react-redux'
import { getError } from '../selectors'
import Unauthorized from '../components/Unauthorized'

const mapStateToProps = state => ({
  error: getError(state),
})

export default connect(
  mapStateToProps,
  null,
)(Unauthorized)
