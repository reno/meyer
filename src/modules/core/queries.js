import gql from 'graphql-tag'

export const getEmployersQuery = gql`
  query {
    employers {
      id
      title
      email
      job
      expiration
      createdAt
    }
  }
`

export const getDetailsQuery = gql`
  query {
    details {
      id
      title
      link
      icon
      order
    }
  }
`

export const getSkillsQuery = gql`
  query {
    skills {
      id
      title
      description
    }
  }
`


export const getExperiencesQuery = gql`
  query {
    experiences {
      id
      type
      period
      title
      employer
      link
      description
    }
  }
`
