import decode from 'jwt-decode'

import store from '../../../utils/store'
import { setError } from '../../core/actions'

export default (props) => {
  const {
    history,
    match: {
      params: {
        token,
      },
    },
  } = props

  store.dispatch(setError(''))

  localStorage.setItem('token', token)

  if (token) {
    try {
      const { user } = decode(token)

      if (user && user.admin) {
        history.push('/admin')

        return null
      }
    } catch (err) {
      store.dispatch(setError(err.message))
    }
  }


  history.push('/')
  return null
}
