import gql from 'graphql-tag'

// eslint-disable-next-line
export const getSkillsQuery = gql`
  query {
    skills {
      id
      title
      image
      description
    }
    employer {
      id
      skillLikes
    }
  }
`
