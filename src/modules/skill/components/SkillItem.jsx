import React from 'react'

import {
  withStyles,
  withTheme,
} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Grid from '@material-ui/core/Grid'
import Grow from '@material-ui/core/Grow'
import Hidden from '@material-ui/core/Hidden'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'

import { HtmlParser } from '../../core/'

const SkillItem = (props) => {
  const {
    classes,
    employer,
    index,
    openSnackBar,
    skill,
    updateEmployerLikesMutation,
    inPlace,
    theme: { transitions: { duration: { shortest } } },
  } = props

  const liked = employer && employer.skillLikes && employer.skillLikes.includes(skill.id)

  const updateSkillLikes = () => {
    openSnackBar(`${liked ? "Ok, you don't think" : 'Great, you think'} ${skill.title} will be useful`)
    updateEmployerLikesMutation({
      variables: {
        id: employer.id,
        fieldName: 'skillLikes',
        likeId: skill.id,
      },
    })
  }

  return (
    <Grid
      container
      justify="center"
      key={skill.id}
    >
      {((index % 2) !== 0) && (
        <Hidden xsDown>
          <Grid item sm={4} md={3}>
            {skill.image && (
              <Grid container justify="center">
                <Card
                  className={classes.rounded}
                >
                  <CardMedia
                    className={classes.media}
                    image={`${process.env.REACT_APP_URL}${process.env.REACT_APP_IMAGE_PATH}${skill.image}`}
                  />
                </Card>
              </Grid>
            )}
          </Grid>
        </Hidden>
      )}
      <Grid
        className={`${((index % 2) === 0) && classes.right}`}
        item
        xs={12}
        sm={8}
        md={6}
      >
        <Typography
          className={classes.title}
          variant="title"
        >
          {skill.title}
        </Typography>
        <div className={classes.button}>
          <IconButton
            onClick={updateSkillLikes}
          >
            <Icon color={liked ? 'secondary' : 'primary'}>thumb_up</Icon>
          </IconButton>
        </div>
        <Grow
          className={classes.item}
          in={inPlace}
          {...((inPlace) && { timeout: (shortest * (index + 1)) })}
        >
          <div>
            <HtmlParser
              content={skill.description}
            />
          </div>
        </Grow>
      </Grid>
      {((index % 2) === 0) && (
        <Hidden xsDown>
          <Grid item sm={4} md={3}>
            {skill.image && (
              <Grid container justify="center">
                <Card
                  className={classes.rounded}
                >
                  <CardMedia
                    className={classes.media}
                    image={`${process.env.REACT_APP_URL}${process.env.REACT_APP_IMAGE_PATH}${skill.image}`}
                  />
                </Card>
              </Grid>
            )}
          </Grid>
        </Hidden>
      )}
    </Grid>
  )
}

const styles = () => ({
  rounded: {
    borderRadius: '50%',
    width: 120,
    height: 120,
    margin: 20,
  },
  media: {
    height: 120,
  },
  right: {
    textAlign: 'right',
  },
  title: {
    display: 'inline-block',
  },
  button: {
    marginLeft: 20,
    display: 'inline-block',
  },
  item: {
    marginBottom: 50,
  },
})

export default withStyles(styles)(withTheme()(SkillItem))
