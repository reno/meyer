import React from 'react'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import {
  AuthorizationWrapper,
  CenterWrapper,
  GetInTouch,
} from '../../core/'
import { SkillItem } from '../'

const Skill = (props) => {
  const {
    data: {
      loading,
      employer,
      skills,
      error,
    },
    inPlace,
  } = props

  return (
    <AuthorizationWrapper error={error}>
      <CenterWrapper>
        {!loading && (
          <Grid item xs={12}>
            {skills && skills.map((skill, index) => (
              <SkillItem
                key={skill.id}
                employer={employer}
                index={index}
                inPlace={inPlace}
                skill={skill}
              />
            ))}
          </Grid>
        )}
      </CenterWrapper>
      <CenterWrapper>
        <GetInTouch
          inPlace={inPlace}
          text="Sounds promising?"
        />
      </CenterWrapper>
    </AuthorizationWrapper>
  )
}

const styles = () => ({
  button: {
    marginLeft: 20,
  },
})

export default withStyles(styles)(Skill)
