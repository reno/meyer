import { connect } from 'react-redux'
import {
  compose,
  graphql,
} from 'react-apollo'
import { getSkillsQuery } from '../queries'
import { updateEmployerLikesMutation } from '../mutations'
import { openSnackBar } from '../../core/actions'
import SkillItem from '../components/SkillItem'

const mapDispatchToProps = dispatch => ({
  openSnackBar: message => dispatch(openSnackBar(message)),
})

export default compose(
  graphql(updateEmployerLikesMutation, {
    name: 'updateEmployerLikesMutation',
    options: {
      refetchQueries: [{
        query: getSkillsQuery,
      }],
    },
  }),
  connect(
    null,
    mapDispatchToProps,
  ),
)(SkillItem)

