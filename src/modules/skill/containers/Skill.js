import { graphql } from 'react-apollo'
import { getSkillsQuery } from '../queries'
import Skill from '../components/Skill'

export default graphql(getSkillsQuery)(Skill)
