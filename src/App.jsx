import React from 'react'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { Helmet } from 'react-helmet'

import { MuiThemeProvider } from '@material-ui/core/styles'
import muiTheme from './utils/muiTheme'

import store from './utils/store'
import { saveState } from './utils/localStorage'
import client from './utils/apollo'
import {
  Layout,
  PrivateRoute,
  Unauthorized,
} from './modules/core'
import { Admin } from './modules/admin'
import { Authentication } from './modules/authentication'


store.subscribe(() => {
  saveState({
    core: {
      navIndex: store.getState().core.navIndex,
    },
  })
})


export default () => (
  <Provider store={store}>
    <ApolloProvider client={client}>
      <React.Fragment>
        <MuiThemeProvider theme={muiTheme}>
          <React.Fragment>
            <Helmet>
              <title>{process.env.REACT_APP_TITLE}</title>
              <meta name="description" content={process.env.REACT_APP_DESCRIPTION} />
              <meta property="og:title" content={process.env.REACT_APP_TITLE} />
              <meta property="og:description" content={process.env.REACT_APP_DESCRIPTION} />
              <meta property="og:type" content="website" />
              <meta property="og:url" content={process.env.REACT_APP_URL} />
              <meta property="og:image" content={`${process.env.REACT_APP_URL}${process.env.REACT_APP_IMAGE}`} />
              <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed:100,300,400,700|Material+Icons" rel="stylesheet" />
              <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png" />
              <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png" />
              <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png" />
              <link rel="manifest" href="/favicons/site.webmanifest" />
              <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#ad1457" />
              <meta name="msapplication-TileColor" content="#ad1457" />
              <meta name="theme-color" content="#ffffff" />
            </Helmet>
            <Router>
              <Switch>
                <Route exact path="/auth/:token?" component={Authentication} />
                <Route exact path="/unauthorized" component={Unauthorized} />
                <PrivateRoute exact path="/admin" component={Admin} />
                <PrivateRoute path="/" component={Layout} />
              </Switch>
            </Router>
          </React.Fragment>
        </MuiThemeProvider>
      </React.Fragment>
    </ApolloProvider>
  </Provider>
)
