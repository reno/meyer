# Online CV

## Technology Stack

- [Prisma](https://www.prisma.io)
- [Apollo](https://www.apollographql.com/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
